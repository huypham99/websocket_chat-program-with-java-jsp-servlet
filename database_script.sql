USE [master]
GO
CREATE DATABASE [PRJ_ChatRoom]
GO
USE [PRJ_ChatRoom]
GO
/****** Object:  Table [dbo].[Account]    Script Date: 11/26/2019 21:58:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Account](
	[email] [nvarchar](50) NOT NULL,
	[password] [nvarchar](50) NOT NULL,
	[username] [nvarchar](50) NOT NULL,
	[fname] [nvarchar](50) NOT NULL,
	[lname] [nvarchar](50) NOT NULL,
	[gender] [bit] NOT NULL,
	[createDate] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[email] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[username] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Account] ([email], [password], [username], [fname], [lname], [gender], [createDate]) VALUES (N'hdphong@yahoo.com', N'123456', N'hdphong', N'Duc phong', N'Hoang', 1, CAST(0x0000AA920013A808 AS DateTime))
INSERT [dbo].[Account] ([email], [password], [username], [fname], [lname], [gender], [createDate]) VALUES (N'hdphong1999@gmail.com', N'123456', N'phong_shadow', N'Duc Phong', N'Hoang', 1, CAST(0x0000AA8F00BEDE80 AS DateTime))
INSERT [dbo].[Account] ([email], [password], [username], [fname], [lname], [gender], [createDate]) VALUES (N'nhmy@gmail.com', N'123456', N'My-xing', N'Huyen My', N'Nguyen', 0, CAST(0x0000AA8F00BF5BE4 AS DateTime))
INSERT [dbo].[Account] ([email], [password], [username], [fname], [lname], [gender], [createDate]) VALUES (N'nmquang1999@gmail.com', N'123456', N'nmquang', N'Minh Quang', N'Nguyen', 1, CAST(0x0000AA8F00BEB0CC AS DateTime))
INSERT [dbo].[Account] ([email], [password], [username], [fname], [lname], [gender], [createDate]) VALUES (N'pqhuy1999@gmail.com', N'123456', N'huy_zz', N'Quang Huy', N'Pham', 1, CAST(0x0000AA8F016A8B7F AS DateTime))
INSERT [dbo].[Account] ([email], [password], [username], [fname], [lname], [gender], [createDate]) VALUES (N'tdquynh@gmail.com', N'123456', N'Quynh-kun', N'Duong Quynh', N'Tran', 0, CAST(0x0000AA8F00BF214C AS DateTime))
INSERT [dbo].[Account] ([email], [password], [username], [fname], [lname], [gender], [createDate]) VALUES (N'tranduyanh@yahoo.com', N'123456', N'trananh', N'Duy Anh', N'Tran', 1, CAST(0x0000AA8F00AB273C AS DateTime))
/****** Object:  Table [dbo].[Message]    Script Date: 11/26/2019 21:58:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Message](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[content] [nvarchar](1000) NOT NULL,
	[createDate] [datetime] NOT NULL,
	[sender] [nvarchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Default [DF__Account__createD__45F365D3]    Script Date: 11/26/2019 21:58:53 ******/
ALTER TABLE [dbo].[Account] ADD  DEFAULT (getdate()) FOR [createDate]
GO
/****** Object:  Default [DF__Message__createD__4AB81AF0]    Script Date: 11/26/2019 21:58:53 ******/
ALTER TABLE [dbo].[Message] ADD  DEFAULT (getdate()) FOR [createDate]
GO
/****** Object:  ForeignKey [FK__Message__sender__4BAC3F29]    Script Date: 11/26/2019 21:58:53 ******/
ALTER TABLE [dbo].[Message]  WITH CHECK ADD FOREIGN KEY([sender])
REFERENCES [dbo].[Account] ([username])
GO
