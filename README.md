# Chat Room Web Socket Project

Chat Room Web Socket Project is written with Java, builds a website allow users to join **Hello World** community and chat together.

## Notes

*  This project must be opened and run by NetBeans IDE.
*  Run ***database_script.sql*** by SQL Server Management Studio and execute to set up database for the application.

## Detailed information to experience the application

*  Account details **(you could register to use a new one)**

        username                |  password
        -------------------------------------
        pqhuy1999@gmail.com     |  123456
        hdphong1999@gmail.com   |  123456
        nmquang1999@gmail.com   |  123456
        
*  After log in, the website would redirect to chat room site.